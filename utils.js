"use strict";
const fs = require("fs");
const path = require("path");

exports.sortDirContents = function(pathString, asc = true) {
  const fullFile = [];
  const files = fs.readdirSync(pathString);
  for (const file of files) {
    try {
      const fileSpec = path.join(pathString, file);
      const stats = fs.statSync(fileSpec);
      if (!stats) return;
      if (stats.isFile()) {
        fullFile.push({ fileName: file, created: stats.mtime });
      }
    } catch (error) {
      console.error(error.message);
    }
  }
  fullFile.sort((a, b) => {
    if (asc) {
      return a.created - b.created;
    } else {
      return b.created - a.created;
    }
  });
  return fullFile.map(x => x.fileName);
};
